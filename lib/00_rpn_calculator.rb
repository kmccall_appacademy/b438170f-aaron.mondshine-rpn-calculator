require 'byebug'

class RPNCalculator
  attr_accessor :calculator


  def initialize
    @stack = []
    @length = 0
  end

  def push(num)
    @stack << num
    @length += 1
  end

  def plus #2,3,4
    self.checker
    popper = @stack.pop
    @stack[-1] += popper
    @length -= 1
  end

  def minus
    self.checker
    popper = @stack.pop
    @stack[-1] -= popper
    @length -= 1
  end

  def divide
    self.checker
    popper = @stack.pop
    @stack[-1] /= popper.to_f
    @length -= 1
  end

  def times
    self.checker
    popper = @stack.pop
    @stack[-1] *= popper
    @length -= 1
  end

  def tokens(str)
    operators = "*-/+"
    str.split.map { |ch| operators.include?(ch) ? ch.to_sym : ch.to_i }
  end

  def evaluate(str)
    self.tokens(str).each do |item|
      case item
      when :+
        self.plus
      when :-
        self.minus
      when :/
        self.divide
      when :*
        self.times
      else
        self.push(item)
      end
    end
    self.value
  end

  def value
    @stack[-1]
  end

  def checker
    raise "calculator is empty" if @length <= 1
  end
end
